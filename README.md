# Fuck Gab!

Fuck Gab! is a simple information web page designed to help people frustrated with Gab find better alternatives.

## How this site runs

The infrastructure this static web page runs on is very simple. It only uses pure HTML and CSS. All the HTML can be found in public.html. And the CSS, images, and fonts are locally loaded from the public folder. Because it doesn't rely on anything other than HTML and CSS, it is very easy to install and can be run by anyone as long as they have a web server such as Nginx or Apache. Though, we recommend that you run this webpage with Nginx.

We provide a sample configuration that you can use to run the page with an Nginx web server in the config.example folder.

## How to add an instance

To add an instance to this web page, simply fork this repo, make your changes, and then [submit a pull request](https://code.sandiamesa.com/traboone/fuck-gab/pulls). All we ask is that the instance does not actively encourage or allow activity and content illegal in the United States to occur.

## Copyright

Copyright (C) 2020 Sandia Mesa Animation Studios<br>
Fuck Gab! is licensed under the [MIT License](https://code.sandiamesa.com/traboone/fuck-gab/src/branch/master/LICENSE).
