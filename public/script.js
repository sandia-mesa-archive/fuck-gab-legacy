function capitalize(text) {
  if (typeof text !== 'string') return '';
  return text.charAt(0).toUpperCase() + text.slice(1)
}

function createEntry(instance, info) {
  const container = document.getElementById('instances-content');

  const instanceContainer = document.createElement('div');
  instanceContainer.className = "tab-item " + instance;

  const title = document.createElement('h3');
  title.className = 'tab-title';
  title.textContent = info.name;
  instanceContainer.appendChild(title);

  const infoContainer = document.createElement('p');
  infoContainer.className = 'tab-info';
  infoLink = document.createElement('a');
  infoLink.href = infoLink.textContent = info.url;
  infoContainer.appendChild(infoLink);
  infoSoftware = document.createElement('span');
  infoSoftware.className = 'tab-software';
  infoSoftwareIcon = document.createElement('img');
  infoSoftwareIcon.className = 'icon';
  infoSoftwareIcon.title = capitalize(info.software) + ' icon';
  infoSoftwareIcon.src = 'images/' + info.software + '-icon.svg';
  infoSoftware.appendChild(infoSoftwareIcon);
  infoSoftware.insertAdjacentHTML('beforeend', capitalize(info.software));
  infoContainer.appendChild(infoSoftware);
  instanceContainer.appendChild(infoContainer);

  container.appendChild(instanceContainer);
}

function search() {
  document.getElementById('instances-content').innerHTML = '<p class="loading">Loading...</p>';

  window.fetch('/instances.json').then((res) => {
    const search = document.getElementById('search');
    const lowerSearch = search.value.toLowerCase();
    if (res.ok) {
      res.json().then((json) => {
        document.getElementById('instances-content').innerHTML = '';

        Object.keys(json).sort().forEach((instance) => {
          if (
            instance.includes(search.value) ||
            json[instance].name.includes(search.value) ||
            json[instance].url.includes(search.value) ||
            json[instance].software.includes(search.value) ||
            json[instance].software.includes(lowerSearch) ||
            search.value === ''
          ) {
            createEntry(instance, json[instance]);
          }
        });
      });
    }
  });
}

document.getElementById('search').onkeyup = search;

search();
