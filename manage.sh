#!/usr/bin/env bash
# This is a bash script for managing Fuck Gab!
# For help with using it, run ./manage.sh --help

# Date suffix function
# Source: https://stackoverflow.com/questions/2495459/formatting-the-date-in-unix-to-include-suffix-on-day-st-nd-rd-and-th#21370675

DaySuffix() {
  case `date +%-d` in
    1|21|31) echo "st";;
    2|22)    echo "nd";;
    3|23)    echo "rd";;
    *)       echo "th";;
  esac
}

# Text colors
RED="\033[1;31m"
GREEN="\033[1;32m"
CLEAR="\033[0m"

# Error and success functions
error() {
  echo -e "${RED}[Error] ${CLEAR}$1"
  exit 1
}

success() {
  echo -e "${GREEN}[OK] ${CLEAR}$1"
}

# Arrays for API endpoints
API_ENDPOINTS=("/api/v1/instance" "/nodeinfo/2.0")
API_INSTANCE_NAME_ENDPOINTS=(".title" ".metadata.nodeName")
API_INSTANCE_SOFTWARE_ENDPOINTS=(".version" ".software.name")

# Response data functions
get_response_data() {
  endpoint_pos=-1
  for i in "${API_ENDPOINTS[@]}"; do
    ((endpoint_pos += 1))
    api_endpoint_received="$i"
    resp="$(curl -s -i -X GET "$url""$i")"
    httpCode="$(echo "$resp" | grep HTTP/ | awk '{print $2}')"

    if [ "$httpCode" = 200 ]; then
      break
    fi
  done

  if [ "$httpCode" != 200 ]; then
    error "Couldn't get anything. Check your URL or your internet connection."
  fi

  success "Got response from ${url}${api_endpoint_received}."
}

set_response_data() {
  instanceName="$(echo "$resp" | grep "{" | jq -r ${API_INSTANCE_NAME_ENDPOINTS[$endpoint_pos]})"
  check_if_instance_value_not_null "$instanceName" "name"

  instanceSoftware="$(echo "$resp" | grep "{" | jq -r ${API_INSTANCE_SOFTWARE_ENDPOINTS[$endpoint_pos]})"
  if [[ "$instanceSoftware" == *"Pleroma"* ]]; then
    instanceSoftware="pleroma"
  elif [ "$instanceSoftware" != "misskey" ]; then
    instanceSoftware="mastodon"
  fi

  check_if_instance_value_not_null "${instanceSoftware^}" "software"
}

check_if_instance_value_not_null() {
  if [ "$1" = "null" ]; then
    error "Got no instance "$2"."
  else
    success "Instance's $2 is $1"
  fi
}

# Task functions
submit() {
  url_exists=$(jq ".[].url | if index( \"$url\" ) then true else false end" public/instances.json)
  if [ -z "$url" ]; then
    error "No URL is specified."
  elif [[ " ${url_exists[@]} " =~ "true" ]]; then
    error "URL already exists."
  fi

  get_response_data
  set_response_data

  read -p "What do you want the slug for the instance to be (e.g. traboone-social)? " instanceSlug

  jq ". += {\"$instanceSlug\": {\"name\": \"$instanceName\",\"url\": \"$url\",\"software\": \"$instanceSoftware\"}}" \
    public/instances.json >public/instances.json.tmp && mv public/{instances.json.tmp,instances.json}
  proceed="To proceed with the submission, commit this change to a forked repo and then create a merge request to the traboone/fuck-gab repo."
  success "The instance ${instanceName} has been written to public/instances.json.\n\n$proceed"
}

update-date() {
  updated_date=$(date +"%B %-d`DaySuffix`, %Y")
  line="<p class=\"last-updated\">"
  newline="<p class=\"last-updated\">Last Updated: ${updated_date}<\/p>"

  sed -i "/${line}/c\					${newline}" public/index.html
}

help() {
  echo -e "${CLEAR}Usage: $(basename "$0") <task> [<options>]

  The tasks that can be executed by this script are:

  submit <url>
    Add a new instance to instances.json.

  update-date
    Update the last updated date to be the current date.
  "
}

# Main program
if [ -z "$1" ] || [ "$1" = "--help" ]; then
  help
else
  task="$1"
  if [ "$2" ] && [ $task = "submit" ]; then
    url="$2"
  fi

  if [ $task = "submit" ] || [ $task = "update-date" ]; then
    $task
  else
    help
  fi
fi
